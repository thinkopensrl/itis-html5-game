
var express = require('express');
var http = require('http');
var app = express();
var server = http.Server(app)
var io = require('socket.io')(server);


var port = process.env.PORT || 3000;

app.use(express.static(__dirname +  '/public'));

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });
});

server.listen(port, function(){
  console.log('listening on *:' + port);
});
